CC=gcc
CFLAGS=-Wall -Werror -pedantic-errors -std=c99

all: fuckify.c
	$(CC) $(CFLAGS) -o brainfuckify $<

clean:
	-rm *.o *~ brainfuckify

.PHONY: tags

tags:
	ctags *.[ch]

