#include <stdio.h>
#include <stdlib.h>


// Converts c to x * y + z,
// counter has length 3.
unsigned char* simpleFactor(unsigned char c){

    unsigned char *counter = (unsigned char*) malloc(3 * sizeof(unsigned char));

    if(!counter){
        fprintf(stderr, "%s", "Malloc failed!\n");
        exit(1);
    }

    counter[0] = 0;
    counter[1] = 1;
    counter[2] = 0;

    // 1.5 is rather arbitrary:
    // Just finish, whenm the values are close enough.
    while(c > 1.5 * counter[1]){

        // Uneven number:
        if(c & 1){
            c -= 1;
            c /= 2;
            counter[2] += counter[1];
            counter[1] *= 2;
        } else {
            c /= 2;
            counter[1] *= 2;
        }
    }

    counter[0] = c;

    return counter;
}


void convert(char* message){

    while(*message){

        // Convert c to x * y + z:
        unsigned char c = *message;
        unsigned char *counter = simpleFactor(c);
        printf(">");

        // First counter:
        for(int i = 0; i < counter[0]; ++i){
            printf("+");
        }

        printf("[<");

        // Second counter:
        for(int i = 0; i < counter[1]; ++i){
            printf("+");
        }

        printf(">-]");
        printf("<");

        // Remainder, if needed:
        for(int i = 0; i < counter[2]; ++i){
            printf("+");
        }

        // End of line:
        printf(".[-]\n");

        free(counter);

        // Rest of message:
        ++message;
    }
}


int main(int argc, char *argv[]){

    // Check for input:
    if(argc == 1){
        printf("Usage:\n ./brainfuckify \"message\"\n");
        exit(0);
    }

    // Print result:
    convert(argv[1]);

    return 0;
}


